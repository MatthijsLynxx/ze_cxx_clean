from src.Preprocess.raw_preprocess import parse_data, routelijst
from src.Modeling.modellen import regressie, train_en_test
from sklearn.ensemble import RandomForestRegressor
from src.Modeling.metrics import test_and_metrics_rf
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
import pandas as ps


from src.settings import RAW_DATA
import pandas as ps
sns.set(style="ticks", color_codes=True)

#TODO Save R2 of regrssion (score)

#Specify current folder
dirname = os.path.dirname(__file__)

# Instellen routes en soorten data
routelijst = routelijst(AML = True, ZOB = True)
all_data, feature_list, dates_list = parse_data(folder_name=os.path.join(dirname, 'Data/01 Raw'),
                                                                       routelijst=routelijst, filter_route = False)

data_x_train, data_y_train, data_x_test, data_y_test = train_en_test(all_data, frac=0.8, random_state=200, save_results=True)

#Extraheer de datums voor latere vergelijking
dates_list_train = dates_list.sample(frac=0.8, random_state=200)
dates_list_test = dates_list.drop(dates_list_train.index)

# # Correlation plots
# corr_plots = sns.pairplot(all_data, x_vars=["verbruik","Nr of Boardings","Avg Occupancy","Distance Trip (KM)","Avg Speed",
#                                      "Avg Urbanisation","Punctuality (depart)","Trip_Duration_Minute",
#                                      "drivestyle_value","Number of stops"], y_vars=["verbruik"])
# corr_plots.savefig(os.path.join(dirname,'Data/03 Final/all_data_train_corr_plots.png'))
# plt.show()


#Regressie met alle dummies
results_df, model, predict_y, errors_test, mse_test = regressie(data_x_train,data_y_train, data_x_test, data_y_test, save_results= True)
print('De MSE is met dummies', mse_test)

#En zonder dummies
data_x_train_zd = data_x_train.iloc[:,0:31]
data_x_test_zd = data_x_test.iloc[:,0:31]

results_df_zd, model_zd, predict_y_zd, errors_test_zd, mse_test_zd = regressie(data_x_train_zd, data_y_train,
                                                                               data_x_test_zd, data_y_test, save_results= True, save_argument='zd')
print('De MSE is zonder dummies', mse_test_zd)

# Plot resultaten (nu met dummies)
plt.scatter(data_y_test, predict_y)
plt.xlabel("Verbruik")
plt.ylabel("Voorspeld verbruik")
plt.title("Prestatie voorspelling verbruik")
plt.show()


#Random Forest
# rf = RandomForestRegressor(n_estimators=1000, criterion='mae', random_state=42, n_jobs=-1, max_features=15,
#                            bootstrap=True, min_samples_leaf=1, min_samples_split=5)
# data_y_train_vector = np.ravel(data_y_train)
# data_y_test_vector = np.ravel(data_y_test)
# rf.fit(data_x_train, data_y_train_vector)
#
# feature_importances_rf, feature_importances_zip_rf, Metrics_List_rf, Predict_array_rf = \
#      test_and_metrics_rf(rf, feature_list, data_x_train, data_y_train_vector, data_x_test, data_y_test_vector)
#
# # Wegschrijven resultaten
# with open(os.path.join(dirname,'feature_importances_rf.txt'), "wb") as fp:
#     pickle.dump(feature_importances_rf, fp)
# with open(os.path.join(dirname,'Metrics_List_rf.txt'), "wb") as fp:
#     pickle.dump(Metrics_List_rf, fp)
# with open(os.path.join(dirname,'Predict_array_rf.txt'), "wb") as fp:
#     pickle.dump(Predict_array_rf, fp)
# with open(os.path.join(dirname, 'dates_list_train.txt'), "wb") as fp:
#     pickle.dump(dates_list_train, fp)
# with open(os.path.join(dirname, 'dates_list_test.txt'), "wb") as fp:
#     pickle.dump(dates_list_test, fp)