from src.Preprocess.raw_preprocess import parse_data, routelijst
from src.Modeling.modellen import regressie, train_en_test
from sklearn.ensemble import RandomForestRegressor
from src.Modeling.metrics import test_and_metrics_rf
import seaborn as sns
import numpy as np
import pickle
import matplotlib.pyplot as plt
import os
import pandas as ps
sns.set(style="ticks", color_codes=True)

#TODO Save R2 of regrssion (score)

#Specify current folder
dirname = os.path.dirname(__file__)

# Instellen routes en soorten data
routelijst = routelijst(AML = True, ZOB = True)
all_data, feature_list, dates_list = parse_data(folder_name=os.path.join(dirname, 'Data/01 Raw'),
                                                                       routelijst=routelijst, filter_route = False)

data_x_train, data_y_train, data_x_test, data_y_test = train_en_test(all_data, frac=0.8, random_state=200, save_results=True)

#Selecteren verbruikdata voor histogram
verbruik_vector = all_data[all_data['verbruik'] < 2.5]
verbruik_vector = verbruik_vector['verbruik']

#Maken histogram van alle verbruikdata
verbruik_plot_regular = sns.distplot(np.asarray(tuple(verbruik_vector))).get_figure()
plt.title('Verdeling van het huidge energieverbruik')
plt.xlabel("Verbruik in kWh per gereden kilometer")
plt.ylabel("Relatieve frequentie")
plt.axvline(x=1.7, ymax=1, color='r', label='Huidige planning')
verbruik_plot_regular.savefig('C:\\Users\\matth\\PycharmProjects\\ZeroEmissionGit\\Data\\04 Output\\' + 'plot_regular' + '.png')
plt.show()

#Berekenen 99ste percentiel van verbruik (huidige plangetal)
print('Het 99ste percentiel van alle data is ',verbruik_vector.quantile(.99))

#Afwijkingen van werkelijk verbruik van huidige plangetal (1.7)
verbruik_vector_afwijkingen_plangetal = verbruik_vector - 1.7

#Histogram afwijkingen
verbruik_plot_afwijkingen_plangetal = sns.distplot(np.asarray(tuple(verbruik_vector_afwijkingen_plangetal))).get_figure()
plt.title('Verdeling van afwijkingen van plangetal (1.7 kWh/km)')
plt.xlabel("Afwijkingen van plangetal in kWh per gereden kilometer")
plt.ylabel("Relatieve frequentie")
plt.axvline(x=0, ymax=1, color='r', label='Huidige planning')
verbruik_plot_afwijkingen_plangetal.savefig('C:\\Users\\matth\\PycharmProjects\\ZeroEmissionGit\\Data\\04 Output\\' + 'plot_afwijkingen_plangetal' + '.png')
plt.show()

#Openen van alle eerder opgeslagen RF output
with open(os.path.join(dirname,'feature_importances_rf.txt'), "rb") as fp:
    feature_importances_rf = pickle.load(fp)
with open(os.path.join(dirname,'Metrics_List_rf.txt'), "rb") as fp:
    Metrics_List_rf = pickle.load(fp)
with open(os.path.join(dirname,'Predict_array_rf.txt'), "rb") as fp:
    Predict_array_rf = pickle.load(fp)
with open(os.path.join(dirname, 'dates_list_train.txt'), "rb") as fp:
    dates_list_train = pickle.load(fp)
with open(os.path.join(dirname, 'dates_list_test.txt'), "rb") as fp:
    dates_list_test = pickle.load(fp)

# Voorspellingen en waarheid uit RF
df_truth_pred_train = ps.DataFrame(Predict_array_rf[0][1], columns=['truth', 'pred'])
df_truth_pred_test = ps.DataFrame(Predict_array_rf[1][1], columns=['truth', 'pred'])

# Datums behorende aan alle losse input regels
datum_series_train = ps.Series(dates_list_train)
datum_series_test = ps.Series(dates_list_test)

# Toevoegen datum aan voorspellingen
df_truth_pred_train['date'] = datum_series_train.values
df_truth_pred_test['date'] = datum_series_test.values

#Test en train samenvoegen
df_truth_pred = ps.concat([df_truth_pred_train,df_truth_pred_test])

#Groeperen en percentielen bepalen per dag.
df_truth_pred_perc = df_truth_pred.groupby('date').quantile(.95)
df_truth_pred_perc.to_csv(os.path.join(dirname,'percentielen.csv'))

# percentielen_pred = df_truth_pred_perc['pred'].values
# percentielen_truth = df_truth_pred_perc['truth'].values
# plancijfer_pred = percentielen_pred.sort()
# plancijfer_truth = percentielen_truth.sort()

# Plot met nieuwe plancijfer toevoegd (1.6)
verbruik_plot_nieuwe_planning = sns.distplot(np.asarray(tuple(verbruik_vector))).get_figure()
plt.title('Verdeling van het huidge energieverbruik')
plt.xlabel("Verbruik in kWh per gereden kilometer")
plt.ylabel("Relatieve frequentie")
plt.axvline(x=1.7, ymax=1, color='r', label='Huidige planning')
plt.axvline(x=1.6, ymax=1, color='g', label='Efficiente planning')
verbruik_plot_nieuwe_planning.savefig('C:\\Users\\matth\\PycharmProjects\\ZeroEmissionGit\\Data\\04 Output\\' + 'plot_regular_efficient' + '.png')
plt.show()

#Dataframe van een normale dag maken
df_truth_goededag = df_truth_pred[df_truth_pred['date'] == '01-07-2017']
df_truth_goededag = df_truth_goededag['truth']

#Plot van normale dag
verbruik_plot_goededag = sns.distplot(np.asarray(tuple(df_truth_goededag))).get_figure()
plt.title('Verdeling van het energieverbruik op een reguliere dag')
plt.xlabel("Verbruik in kWh per gereden kilometer")
plt.ylabel("Relatieve frequentie")
plt.axvline(x=1.6, ymax=1, color='g', label='Efficiente planning')
plt.axvline(x=1.7, ymax=1, color='r', label='Huidige planning')
verbruik_plot_goededag.savefig('C:\\Users\\matth\\PycharmProjects\\ZeroEmissionGit\\Data\\04 Output\\' + 'plot_goededag' + '.png')
plt.show()

#Idem slechte dag
df_truth_slechtedag = df_truth_pred[df_truth_pred['date'] == '12-07-2017']
df_truth_slechtedag = df_truth_slechtedag['truth']

verbruik_plot_slechtedag = sns.distplot(np.asarray(tuple(df_truth_slechtedag))).get_figure()
plt.title('Verdeling van het energieverbruik op een niet-zuinige dag')
plt.xlabel("Verbruik in kWh per gereden kilometer")
plt.ylabel("Relatieve frequentie")
plt.axvline(x=1.6, ymax=1, color='g', label='Efficiente planning')
plt.axvline(x=1.7, ymax=1, color='r', label='Huidige planning')
plt.axvline(x=2, ymax=1, color='grey', label='Noodweer planning')
verbruik_plot_slechtedag.savefig('C:\\Users\\matth\\PycharmProjects\\ZeroEmissionGit\\Data\\04 Output\\' + 'plot_slechtedag' + '.png')
plt.show()


#Percentielen berekenen buiten RF om. Maakt gebruik van de all_data output van de parser.
verbruik_met_datum = ps.DataFrame(all_data['verbruik'])
df_datums = ps.Series(dates_list)
verbruik_met_datum['date'] = df_datums.values

alle_percentielen = verbruik_met_datum.groupby('date').quantile(.95)
alle_counts = verbruik_met_datum.groupby('date').agg(['mean', 'count'])
alle_cijfers = alle_percentielen.merge(alle_counts, how = "right", on = 'date')
alle_cijfers.to_csv(os.path.join(dirname,'alle_percentielen.csv'))