"""
Contains setting variables that are global and never adjusted
"""

import os

# data folder by scenario dependent, not synced with git
DATA_FOLDER = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'Data')
SRC_FOLDER = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'src')

# data pipeline
SUPPORTING_DATA = os.path.join(DATA_FOLDER, '00 supporting_data')
INPUT_DATA = os.path.join(DATA_FOLDER, '00 Input')
RAW_DATA = os.path.join(DATA_FOLDER, '01 Raw')
PREPROCESSED_DATA = os.path.join(DATA_FOLDER, '02 Preprocessed')
OUTPUT_DATA = os.path.join(DATA_FOLDER, '04 Output')
FINAL_DATA = os.path.join(DATA_FOLDER, '03 Final')

