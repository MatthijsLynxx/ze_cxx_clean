import numpy as np
import pandas as ps
from sklearn import metrics
from src.Preprocess.raw_preprocess import scale_dataframe_columns
import seaborn as sns
sns.set(style="ticks", color_codes=True)

def test_and_metrics_rf(rf, feature_list, data_x_train, data_y_train, data_x_dev, data_y_dev):

    Metrics_array = []
    Predict_array = []

    predictions_train = rf.predict(data_x_train)
    predictions_dev = rf.predict(data_x_dev)
    errors_train = metrics.mean_squared_error(data_y_train, predictions_train)  # abs(predictions - data_y_dev)
    errors_dev = metrics.mean_squared_error(data_y_dev, predictions_dev)  # abs(predictions - data_y_dev)
    errors_train_r2 = metrics.r2_score(data_y_train, predictions_train)  # abs(predictions - data_y_dev)
    errors_dev_r2 = metrics.r2_score(data_y_dev, predictions_dev)  # abs(predictions - data_y_dev)
    Predict_array.append(['Train',list(zip(*[np.transpose(data_y_train).tolist(), np.transpose(predictions_train).tolist()]))])
    # Predict_array[1] = ['Dev', [data_y_dev.transpose, predictions_dev.transpose]]
    Predict_array.append(['Dev', list(zip(*[np.transpose(data_y_dev).tolist(), np.transpose(predictions_dev).tolist()]))])
    # Metrics_array[0] = ['MSE train', errors_train]
    Metrics_array.append(['MSE train', round(np.mean(errors_train), 4)])
    Metrics_array.append(['MSE dev', round(np.mean(errors_dev), 4)])
    Metrics_array.append(['R2 train', round(np.mean(errors_train_r2), 4)])
    Metrics_array.append(['r2 dev', round(np.mean(errors_dev_r2), 4)])
    # Print out the mean absolute error (mae)
    print('MSE', round(np.mean(errors_train), 4), 'kwh.')
    print('MSE', round(np.mean(errors_dev), 4), 'kwh.')
    print('R2', round(np.mean(errors_train_r2), 4))
    print('R2', round(np.mean(errors_dev_r2), 4))

    predictions_train_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': predictions_train}), ['Column1'])[0].values
    predictions_dev_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': predictions_dev}), ['Column1'])[0].values
    data_y_train_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': data_y_train}), ['Column1'])[0].values #['Trip_Energy_Per_Km']
    data_y_dev_scaled = scale_dataframe_columns(ps.DataFrame({'Column1': data_y_dev}), ['Column1'])[0].values
    errors_train_scaled = metrics.mean_squared_error(data_y_train_scaled, predictions_train_scaled)  # abs(predictions - data_y_dev)
    errors_dev_scaled = metrics.mean_squared_error(data_y_dev_scaled, predictions_dev_scaled)   # abs(predictions - data_y_dev)
    errors_train_r2_scaled = metrics.r2_score(data_y_train_scaled, predictions_train_scaled)   # abs(predictions - data_y_dev)
    errors_dev_r2_scaled = metrics.r2_score(data_y_dev_scaled, predictions_dev_scaled)   # abs(predictions - data_y_dev)

    print('MSE scaled', round(np.mean(errors_train_scaled), 4), 'kwh.')
    print('MSE scaled', round(np.mean(errors_dev_scaled), 4), 'kwh.')
    print('R2 scaled', round(np.mean(errors_train_r2_scaled), 4))
    print('R2 scaled', round(np.mean(errors_dev_r2_scaled), 4))
    Predict_array.append(['Train - scaled', list(zip(*[np.transpose(data_y_train_scaled).tolist(), np.transpose(predictions_train_scaled).tolist()]))])
    Predict_array.append(['Dev - scaled', list(zip(*[np.transpose(data_y_dev_scaled).tolist(), np.transpose(predictions_dev_scaled).tolist()]))])
    Metrics_array.append(['MSE train - scaled', round(np.mean(errors_train_scaled), 4)])
    Metrics_array.append(['MSE dev - scaled', round(np.mean(errors_dev_scaled), 4)])
    Metrics_array.append(['R2 train - scaled', round(np.mean(errors_train_r2_scaled), 4)])
    Metrics_array.append(['r2 dev - scaled', round(np.mean(errors_dev_r2_scaled), 4)])

    # Get numerical feature importances
    importances = list(rf.feature_importances_)
    # List of tuples with variable and importance
    feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
    # Sort the feature importances by most important first
    feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)
    # Print out the feature and importances

    feature_importances_zip = list(zip(*feature_importances))

    return feature_importances, feature_importances_zip, Metrics_array, Predict_array