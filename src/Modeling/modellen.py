from sklearn.ensemble import RandomForestRegressor
from src.Modeling.metrics import test_and_metrics_rf
import pandas as ps
from sklearn import linear_model
import numpy as np
import os
import seaborn as sns
sns.set(style="ticks", color_codes=True)

dirname = os.path.dirname(__file__)

from src.settings import FINAL_DATA

# Splitten train en test
def train_en_test(all_data, frac=0.8, random_state=200, save_results=True):
    all_data_train = all_data.sample(frac=frac, random_state=random_state)
    all_data_test = all_data.drop(all_data_train.index)
    data_x_train = all_data_train.loc[:, all_data_train.columns != 'verbruik']
    data_y_train = all_data_train.loc[:, all_data_train.columns == 'verbruik']
    data_x_test = all_data_test.loc[:, all_data_test.columns != 'verbruik']
    data_y_test = all_data_test.loc[:, all_data_test.columns == 'verbruik']

    # Opslaan data als een tabel
    if save_results:
        all_data.to_csv('C:\\Users\\matth\\PycharmProjects\\ZeroEmissionGit\\Data\\02 Preprocessed\\all_data_train.csv')

    return data_x_train, data_y_train, data_x_test, data_y_test
def regressie(data_x, data_y, test_x, test_y, save_results=True, save_argument=''):

    reg = linear_model.LinearRegression(fit_intercept=False, normalize=True)
    reg.fit(data_x, data_y)
    print(reg.intercept_)

    # results_df = ps.DataFrame(list(zip(data_x.columns, reg.coef_)), columns=['features', 'estimatedCoefficients'])
    results_df = ps.DataFrame({'features':data_x.columns, 'coefs':reg.coef_[0]})
    model = reg

    predict_y = reg.predict(test_x) #.values.reshape(1, -1))
    errors_test = test_y - predict_y
    mse_test = np.mean((test_y - predict_y)**2)


    if save_results:
        results_df.to_csv(os.path.join(FINAL_DATA,save_argument + '.csv'))
    return results_df, model, predict_y, errors_test, mse_test

#Plot resultaten oude regressie
# results_df = results_df.iloc[2:]
# fig, ax = plt.subplots(figsize=(8, 5))
# results_df.plot(x='varnames', y='coeff', kind='bar',
#              ax=ax, color='none',
#              yerr='sderrors', legend=False)
# ax.set_ylabel('')
# ax.set_xlabel('')
# ax.scatter(x=ps.np.arange(results_df.shape[0]),
#            marker='s', s=20,
#            y=results_df['coeff'], color='black')
# ax.axhline(y=0, linestyle='--', color='black', linewidth=2)
# ax.xaxis.set_ticks_position('none')
# plt.show()
# fig.savefig(os.path.join('Data/04 Output', 'coefficienten_met_error_met_dummies.png'))



