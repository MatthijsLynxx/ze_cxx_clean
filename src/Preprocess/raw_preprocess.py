import numpy as np
import pandas as ps
from sklearn.impute import SimpleImputer
import os
import glob
import seaborn as sns
sns.set(style="ticks", color_codes=True)

def routelijst(AML = True, ZOB = True):
    if AML and ZOB:
        route_base_list = ['M090', 'M095', 'M180', 'M185', 'M186', 'M187', 'M190', 'M191', 'M194', 'M195', 'M198',
                           'M199', 'M242', 'M342', 'M181', 'M347', 'M357', 'L400', 'L401', 'L402', 'L403', 'L404',
                           'L405', 'L406', 'L407']
    elif AML and not ZOB:
        route_base_list = ['M090', 'M095', 'M180', 'M185', 'M186', 'M187', 'M190', 'M191', 'M194', 'M195', 'M198',
                           'M199', 'M242', 'M342', 'M181',  'M347', 'M357']
    elif ZOB and not AML:
        route_base_list = ['L400', 'L401', 'L402', 'L403', 'L404', 'L405', 'L406', 'L407']
    else: print('Geen geldige routes gevonden')

    return route_base_list


def parse_data(folder_name, routelijst = routelijst, filter_route=False):
    #Takes: datafile
    #Returns: numpy array of datafile, formatted / parsed

    if filter_route:
        route = routelijst[0]

    filenames = glob.glob(folder_name + "/*.csv")

    dfs = []
    for filename in filenames:
        bestand = ps.read_csv(filename, sep =';', parse_dates=['Date'])
        dfs.append(bestand)

    # Concatenate all data into one DataFrame
    data = ps.concat(dfs, ignore_index=True)
    print("Aantal regels bij inlezen: %s,%s" % (data.shape))


    # Toevoegen datumvariabelen
    data['Date'] = ps.to_datetime(data['Date'], format='%Y-%m-%d', errors='coerce')
    data['Month'] = ps.DatetimeIndex(data['Date']).month
    data['Weekday'] = ps.DatetimeIndex(data['Date']).weekday

    # Filter regels waarbij energy per km is te laag
    data = data.dropna()
    print("Aantal regels na NaN drop: %s,%s" % (data.shape))

    if filter_route:
        data = data.loc[data['Route'] == route]
        data = [
            data.loc[:, 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Km_Travelled_DuringTrip'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Syc_Speed_kmh'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Urban_Avg'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Departure_Punctuality'].replace(',', '.', regex=True).astype(float),
            data.loc[:, "Trip_Energy_Per_Km"].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Start_Hour'],
            data.loc[:, 'Trip_Duration_Minute'],
            data.loc[:, 'Hour_Temperature'],
            data.loc[:, 'drivestyle_value'],
            data.loc[:, 'Number_of_stops'],
            data.loc[:, 'Month'],
            data.loc[:, 'Weekday'],
            data.loc[:, 'Date']]
    else:
        data = data.loc[data['Route'].isin(routelijst)]
        data = [
            data.loc[:, 'Trip_total_number_Of_Boardings'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Avg_Occupancy'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Km_Travelled_DuringTrip'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Syc_Speed_kmh'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Urban_Avg'].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Trip_Departure_Punctuality'].replace(',', '.', regex=True).astype(float),
            data.loc[:, "Trip_Energy_Per_Km"].replace(',', '.', regex=True).astype(float),
            data.loc[:, 'Start_Hour'],
            data.loc[:, 'Trip_Duration_Minute'],
            data.loc[:, 'Hour_Temperature'],
            data.loc[:, 'drivestyle_value'],
            data.loc[:, 'Number_of_stops'],
            data.loc[:, 'Month'],
            data.loc[:, 'Weekday'],
            data.loc[:, 'Route'],
            data.loc[:, 'Date']
        ]


    data = ps.concat(data,axis=1)

    data = data.loc[data['Trip_Energy_Per_Km'] > 0.05]
    print("Aantal regels na verwijderen laag verbruik: %s,%s" % (data.shape))
    data = data.loc[data['Trip_Energy_Per_Km'] < 3]
    print("Aantal regels na verwijderen hoog verbruik: %s,%s" % (data.shape))
    data = data.loc[data['Trip_Km_Travelled_DuringTrip'] > 1]
    data = data.loc[data['Trip_Duration_Minute'] > 2]
    print("Aantal regels na verwijderen lage km's en lage trip_duur: %s,%s" % (data.shape))



    data = data.rename(index=str, columns={"Trip_total_number_Of_Boardings": "Nr of Boardings", "Trip_Avg_Occupancy": "Avg Occupancy", "Syc_Speed_kmh": "Avg Speed"
        , "Urban_Avg": "Avg Urbanisation", "Trip_Departure_Punctuality": "Punctuality (depart)"
        , "Number_of_stops": "Number of stops", "Trip_Km_Travelled_DuringTrip": "Distance Trip (KM)"})

    data_x = data.loc[:, data.columns != "Trip_Energy_Per_Km"]
    data_y = data.loc[:, "Trip_Energy_Per_Km"]

    data_x = ps.concat([data_x, ps.get_dummies(data_x['Start_Hour'], prefix='StartHour')], axis=1)
    data_x = ps.concat([data_x, ps.get_dummies(data_x['Month'], prefix='Month')], axis=1)
    data_x = ps.concat([data_x, ps.get_dummies(data_x['Weekday'], prefix='Weekday')], axis=1)

    dates_list = data_x['Date']
    data_x = data_x.drop('Start_Hour', axis=1)
    data_x = data_x.drop('Month', axis=1)
    data_x = data_x.drop('Weekday', axis=1)
    data_x = data_x.drop('Date', axis =1)
    if filter_route:
        pass
    else:
        data_x = ps.concat([data_x, ps.get_dummies(data_x['Route'], prefix='Route')], axis=1)
        data_x = data_x.drop('Route', axis=1)


    # Get the headers as a feature list
    feature_list = list(data_x.columns)

    all_data = data_x.assign(verbruik=data_y.values)

    # imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    # data_x = imp.fit_transform(data_x)

    return all_data, feature_list, dates_list

def scale_dataframe_columns(frame: ps.DataFrame, columnList):
    sdevs = frame[ columnList ].std()
    means =  frame[columnList ].mean()
    scaledframe = frame
    scaledframe[columnList ] -= means
    scaledframe[columnList ] /= sdevs
    return scaledframe, means, sdevs